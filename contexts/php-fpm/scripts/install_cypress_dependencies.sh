#!/bin/bash

set -eu

apt-get install -y libgtk2.0-0 \
            libgtk-3-0 \
            libgbm-dev \
            libnotify-dev \
            libgconf-2-4 \
            libnss3 \
            libxss1 \
            libasound2 \
            libxtst6 \
            xauth \
            gnupg2 \
            xvfb

wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
echo "deb http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google.list
apt-get update
apt-get install -y dbus-x11 google-chrome-stable
