# docker-compose-collection

Inspired by [laradock](https://laradock.io) this project contains a useful docker-compose.yml file for working with php, codeception, laravel, .... I preferred creating a collection by myself because there were several things I couldn't do with laradock.

# Usage
## Via docker-compose
To use this project you just have to do 3 steps:
1. clone it
2. copy .env.example to .env (or use ```./bin/init.sh```)
3. run ```./bin/start.sh```

## Via docker swarm
1. clone it
2. copy .env.example to .env
3. join or create a docker swarm (see [https://docs.docker.com/engine/swarm/](https://docs.docker.com/engine/swarm/) for more details)
4. run ```docker stack deploy -c <(./bin/config.sh) my_fancy_app```

# Docs
You can found detailed information about all included containers, configuration and customization in the [wiki](https://gitlab.com/Bosi/docker-compose-collection/wikis/home).
