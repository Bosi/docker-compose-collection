#!/usr/bin/env bats

load helpers

run_script () {
    run ./bin/subscripts/docker-compose.sh config
    [ $status -eq 0 ]
}

run_script_restart () {
    run ./bin/restart.sh mailhog
    [ $status -eq 0 ]
}

@test $(get_description "check if NETWORK_SUBNET is optional") {
    run_script
    [ $(echo $output | grep -i "subnet" | wc -l) -eq 0 ]

    run_script_restart

    sed -i "s|^\(NETWORK_SUBNET=\s*\).*$|\110.123.0.2/16|" .env
    run_script
    [ $(echo $output | grep -i "subnet: 10.123.0.2/16" | wc -l) -eq 1 ]

    run_script_restart

    run ./bin/subscripts/docker-compose.sh ps
    [ $status -eq 0 ]
}
