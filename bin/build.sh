#!/usr/bin/env bash
RUNTIME_START=$(date +%s)

DIR="$(dirname ${0})/../"
cd ${DIR}

./bin/subscripts/check_env.sh
if [[ $? -ne 0 ]]; then exit 1; fi

COMPOSE_PROJECT_NAME=$(grep -r COMPOSE_PROJECT_NAME .env | cut -d= -f2)
PROJECT_IMAGES=$(docker images | grep ${COMPOSE_PROJECT_NAME})

. ./bin/subscripts/services.sh

echo "Build the following services: ${SERVICES}"
./bin/subscripts/docker-compose.sh build --no-cache ${SERVICES}

printf "INFO: check for old images \n"
for ITEM in $(docker images --filter "dangling=true" -q); do
    if [[ $(echo ${PROJECT_IMAGES} | grep ${ITEM}) != "" ]]; then
        printf "INFO: remove image with id ${ITEM} \n"
        docker rmi -f ${ITEM}
    fi
done

cd - > /dev/null

printf "INFO: script ${0} runtime: $(($(date +%s)-RUNTIME_START)) second(s)\n"
