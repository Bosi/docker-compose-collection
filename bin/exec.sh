#!/usr/bin/env bash
RUNTIME_START=$(date +%s)

if [[ -z ${1} ]]; then
    echo "you have to set a command via command param"
    exit 1
fi

DIR="$(dirname ${0})/../"
cd ${DIR}

./bin/subscripts/check_env.sh
if [[ $? -ne 0 ]]; then exit 1; fi

COMPOSE_PROJECT_NAME=$(grep -r COMPOSE_PROJECT_NAME .env | cut -d= -f2)
docker exec -t --user=www-data "${COMPOSE_PROJECT_NAME}_php-fpm_1" ${@}
RETURN_CODE=$?
cd - > /dev/null

printf "INFO: script ${0} runtime: $(($(date +%s)-RUNTIME_START)) second(s)\n"

exit ${RETURN_CODE}
