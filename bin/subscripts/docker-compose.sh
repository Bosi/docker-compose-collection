#!/usr/bin/env bash

check() {
    DOCKER_COMPOSE_FILE_PARAM=${1}
    CONFIG_KEY=${2}
    DOCKER_COMPOSE_FILENAME=${3}

    if [[ ! -z $(grep -r "${CONFIG_KEY}=" .env | cut -d= -f2) ]]; then
        DOCKER_COMPOSE_FILE_PARAM="${DOCKER_COMPOSE_FILE_PARAM} -f ${DOCKER_COMPOSE_FILENAME}"
    fi

    echo -n ${DOCKER_COMPOSE_FILE_PARAM}
}

./bin/subscripts/check_env.sh
if [[ $? -ne 0 ]]; then exit 1; fi

DOCKER_COMPOSE_FILE_PARAM="-f docker-compose.yml"
if [[ $(grep -r LINK_PORTS .env | cut -d= -f2) == "true" ]]; then
    DOCKER_COMPOSE_FILE_PARAM="${DOCKER_COMPOSE_FILE_PARAM} -f docker-compose-ports.yml"
fi

DOCKER_COMPOSE_FILE_PARAM=$(check "${DOCKER_COMPOSE_FILE_PARAM}" "NETWORK_SUBNET" "docker-compose-network_backend.yml")
DOCKER_COMPOSE_FILE_PARAM=$(check "${DOCKER_COMPOSE_FILE_PARAM}" "FRONTEND_NETWORK_NAME" "docker-compose-network_frontend.yml")
DOCKER_COMPOSE_FILE_PARAM=$(check "${DOCKER_COMPOSE_FILE_PARAM}" "VIRTUAL_HOST_NGINX" "docker-compose-virtual_host_nginx.yml")
DOCKER_COMPOSE_FILE_PARAM=$(check "${DOCKER_COMPOSE_FILE_PARAM}" "VIRTUAL_HOST_APACHE" "docker-compose-virtual_host_apache.yml")
DOCKER_COMPOSE_FILE_PARAM=$(check "${DOCKER_COMPOSE_FILE_PARAM}" "VIRTUAL_HOST_MAILHOG" "docker-compose-virtual_host_mailhog.yml")
DOCKER_COMPOSE_FILE_PARAM=$(check "${DOCKER_COMPOSE_FILE_PARAM}" "VIRTUAL_HOST_PHPMYADMIN" "docker-compose-virtual_host_phpmyadmin.yml")
DOCKER_COMPOSE_FILE_PARAM=$(check "${DOCKER_COMPOSE_FILE_PARAM}" "LETSENCRYPT_EMAIL" "docker-compose-letsencrypt.yml")

if [[ -f "docker-compose-override.yml" ]]; then
    DOCKER_COMPOSE_FILE_PARAM="${DOCKER_COMPOSE_FILE_PARAM} -f docker-compose-override.yml"
fi

docker-compose ${DOCKER_COMPOSE_FILE_PARAM} ${@}