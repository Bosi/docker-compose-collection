#!/usr/bin/env bash
DIR="$(dirname ${0})/../../"

cd ${DIR}

if [[ -f ".env.override" ]]; then
    ENVS=".env.override"
    rm -f .env
fi

if [[ -f ".env" ]]; then
    ENVS=".env ${ENVS}"
fi

awk -F= '!a[$1]++' ${ENVS} .env.example >.env.2
mv .env.2 .env

TIMEZONE=$(grep -r TIMEZONE .env | cut -d= -f2)
ls -l "/usr/share/zoneinfo/${TIMEZONE}" >/dev/null
if [[ $? -ne 0 ]]; then
    printf "ERROR: the timezone you specified could not be found\n"
    exit 1
fi

USER_ID=$(grep -r UID .env | cut -d= -f2)
if [[ ${USER_ID} -eq 0 ]]; then
    printf "ERROR: the user id must not be 0 (root user id)\n"
    exit 1
fi

if [[ ! -f ./bin/external/yq ]]; then
    if [[ "$OSTYPE" == "darwin"* ]]; then
        # macOS
        curl -sL https://github.com/mikefarah/yq/releases/download/v4.24.5/yq_darwin_amd64 -o ./bin/external/yq
    else
        # linux
        curl -sL https://github.com/mikefarah/yq/releases/download/v4.24.5/yq_linux_amd64 -o ./bin/external/yq
    fi

    chmod +x ./bin/external/yq
fi
